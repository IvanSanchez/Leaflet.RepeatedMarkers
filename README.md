# Leaflet.RepeatedMarkers

A [LeafletJS](http://leafletjs.com/) plugin to display markers when wrapping around the globe, once every 360 degrees of longitude.

This leverages Leaflet's `GridLayer`, so that each marker is shown once for each
zoom-level-zero tile.

See a [live demo](https://ivansanchez.gitlab.io/Leaflet.RepeatedMarkers/demo.html).

![Screenshot showing this plugin in action](screenshot.png)

## Usage

Include the Leaflet JS and CSS in your HTML page:

```html
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.3/dist/leaflet.css" />
<script src="https://unpkg.com/leaflet@1.3.3/dist/leaflet.js"></script>
```

Include the Leaflet.RepeatedMarkers javascript file too:

```html
<script src='https://unpkg.com/leaflet.repeatedmarkers@latest/Leaflet.RepeatedMarkers.js'></script>
```

Then, you can create an instance of `L.GridLayer.RepeatedMarkers` on your JS code. Note that `RepeatedMarkers` is a subclass of `L.GridLayer`, hence according to the Leaflet conventions its class name is `L.GridLayer.RepeatedMarkers` and its factory method is `L.gridLayer.repeatedMarkers`:

```javascript
var myRepeatingMarkers = L.gridLayer.repeatedMarkers().addTo(map);
```

Then, use the `addMarker()` method to add some markers:

```javascript
myRepeatingMarkers.addMarker(   L.marker([0, 0])   );
```

If you store references to your markers, then you can also use the `removeMarker()` method later:

```javascript
var nullIslandMarker = L.marker([0, 0]);
myRepeatingMarkers.addMarker( nullIslandMarker  );
setTimeout(function(){
    myRepeatingMarkers.removeMarker( nullIslandMarker );
}, 10000);
```


## Possible FAQ

* "How do I do this but for lines/polygons?"

This is only for point markers. The code *could* be adapted to support `L.Polyline`s and `L.Polygon`s and nested `L.LayerGroup`s, but that would require some extra work. If you're willing to do such adaptation, then merge requests are welcome.

Also, [Leaflet.VectorGrid.Slicer](https://github.com/Leaflet/Leaflet.VectorGrid) already provides similar functionality for lines/polygons.

## Legalese

Licensed under LGPL3. Because why not. See the LICENSE file for details.

